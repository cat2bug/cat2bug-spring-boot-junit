package com.cat2bug.junit.demo;

import com.cat2bug.junit.Cat2BugAutoSpringSuite;
import com.cat2bug.junit.Cat2BugRunner;
import com.cat2bug.junit.Cat2BugSpringRunner;
import com.cat2bug.junit.annotation.Authentication;
import com.cat2bug.junit.annotation.AutoTestScan;
import com.cat2bug.junit.annotation.PushReport;
import com.cat2bug.junit.annotation.RandomParameter;
import com.cat2bug.junit.demo.entity.User;
import com.cat2bug.junit.demo.service.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest
@RunWith(Cat2BugAutoSpringSuite.class)
@AutoTestScan(packageName = "com.cat2bug.junit.demo.controller")
@PushReport(host = "http://127.0.0.1:2020", projectKey = "20240225012438h19zzdb6sd1ahazj", handler = "admin")
@Authentication(name = "admin",password = "cat2bug")
@Transactional
public class Cat2bugSpringBootDemoApplicationTest {
    /**
     * 插入用户
     * @param userRepository
     * @return
     */
    @RandomParameter(className = "com.cat2bug.junit.demo.controller.UserController", methodName = "updateUser", parameterName = "user")
    public User insertUser(
            @Autowired UserRepository userRepository
    ) {
        User user = new User(1L,"admin","cat2bug");
        return userRepository.save(user);
    }

    /**
     * 计算接口方法中，参数名包含Id的字符型的返回值。
     *
     * @return 用户ID
     */
    @RandomParameter(parameterName = ".*Id.*")
    public long userId() {
        return 1L;
    }

    /**
     * 计算接口方法中，参数名等于name的的返回值。
     *
     * @return
     */
    @RandomParameter(parameterName = "name")
    public String name() {
        String[] names = { "唐玄奘", "孙悟空", "猪八戒", "沙悟净" };
        return names[(int) (Math.random() * names.length)];
    }

    @Test
    public void testRuleTrue() {
        Assert.assertTrue(true);
    }
}
