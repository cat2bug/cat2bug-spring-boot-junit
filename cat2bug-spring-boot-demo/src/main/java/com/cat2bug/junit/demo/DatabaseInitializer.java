package com.cat2bug.junit.demo;

import com.cat2bug.junit.demo.entity.User;
import com.cat2bug.junit.demo.service.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @Author: yuzhantao
 * @CreateTime: 2024-03-29 19:52
 * @Version: 1.0.0
 * 初始化数据库
 */
@Component
public class DatabaseInitializer implements CommandLineRunner {

    private final UserRepository userRepository;

    public DatabaseInitializer(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) {
        User user = new User();
        user.setId(1L);
        user.setName("admin");
        user.setPassword("cat2bug");
        userRepository.save(user);

    }
}