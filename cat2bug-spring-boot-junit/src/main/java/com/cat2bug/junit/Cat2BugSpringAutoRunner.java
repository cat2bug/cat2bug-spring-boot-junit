package com.cat2bug.junit;

import com.cat2bug.junit.listener.Cat2BugAutoRunListener;
import com.cat2bug.junit.listener.Cat2BugRunListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 基于Cat2Bug的自动SpringRunner
 */
public class Cat2BugSpringAutoRunner extends SpringJUnit4ClassRunner {
	private Class<?> testClass;

	public Cat2BugSpringAutoRunner(Class<?> clazz) throws InitializationError {
		super(clazz);
		this.testClass = clazz;
	}

	@Override
	public void run(RunNotifier notifier) {
		notifier.addListener(new Cat2BugAutoRunListener());
		super.run(notifier);
	}
}
