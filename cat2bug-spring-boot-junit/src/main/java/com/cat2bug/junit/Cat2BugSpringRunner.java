package com.cat2bug.junit;

import com.cat2bug.junit.listener.Cat2BugRunListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 基于Cat2Bug的SpringRunner
 */
public class Cat2BugSpringRunner extends SpringJUnit4ClassRunner {
	private Class<?> testClass;

	public Cat2BugSpringRunner(Class<?> clazz) throws InitializationError {
		super(clazz);
		this.testClass = clazz;
	}

	@Override
	public void run(RunNotifier notifier) {
		notifier.addListener(new Cat2BugRunListener(this.testClass));
		super.run(notifier);
	}
}
