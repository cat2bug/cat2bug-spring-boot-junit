package com.cat2bug.junit.service;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 编译类结果服务
 */
public class CompileClassResultService {
    private static Map<Class<?>,Object> successClassMap = new ConcurrentHashMap<>();

    private static Map<Class<?>,Throwable> failClassMap = new ConcurrentHashMap<>();

    public static void addSuccessClass(Class<?> cls) {
        successClassMap.put(cls,"");
    }

    public static void addFailClass(Class<?> cls, Throwable t) {
        failClassMap.put(cls,t);
    }

    public static Set<Class<?>> getSuccessResults() {
        return successClassMap.keySet();
    }

    public static Map<Class<?>,Throwable> getFailResults() {
        return failClassMap;
    }
}
