package com.cat2bug.junit.service.string;

import com.cat2bug.junit.service.IRandomStringService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 创建随机字符串的服务
 */
public class CharRandomStringService implements IRandomStringService {
    private static final String[] GENERATE_SOURCE = new String[]{"0", "1", "2", "3", "4", "5", "6", "7",
            "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z"};
    private static final int STR_LEN = GENERATE_SOURCE.length;

    @Override
    public String getRandomString() {
        List<String> list = Arrays.asList(GENERATE_SOURCE);
        //打乱元素排序，增加反推难度
        Collections.shuffle(list);
        StringBuilder randomStr = new StringBuilder();
        for (int i = 0; i < STR_LEN; i++) {
            randomStr.append(list.get(i));
        }
        return randomStr.substring(5, 10);
    }
}
