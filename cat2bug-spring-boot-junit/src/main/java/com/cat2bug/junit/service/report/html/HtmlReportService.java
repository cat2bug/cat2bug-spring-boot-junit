package com.cat2bug.junit.service.report.html;

import com.cat2bug.junit.service.IReport;
import com.cat2bug.junit.service.report.AbstractHtmlReport;
import com.cat2bug.junit.service.report.AbstractTableReport;
import com.cat2bug.junit.util.AppUtils;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Html报告服务
 */
public class HtmlReportService extends AbstractHtmlReport {
    private List<AbstractTableReport> reportList = new ArrayList<>();

    public HtmlReportService(AbstractTableReport... list) {
        reportList.addAll(Arrays.stream(list).collect(Collectors.toList()));
    }

    @Override
    public String getTitle() {
        return "本地测试报告";
    }

    @Override
    protected String getTemplateName() {
        return "index.ftl";
    }

    @Override
    protected String getOutputFileName() {
        return "index.html";
    }

    @Override
    protected Map getData() {
        Map dataModel = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dataModel.put("reportTime",sdf.format(new Date()));
        dataModel.put("cat2bugVersion", AppUtils.getAppVersion());
        dataModel.put("reports",this.reportList.stream().map(r->{
            Map<String,Object> ret = new HashMap<>();
            ret.put("title",r.getTitle());
            ret.put("tableHeader",r.getTableHeader());
            ret.put("tableData",r.getTableData());
            return ret;
        }).collect(Collectors.toList()));
        return dataModel;
    }

    @Override
    protected String getReport(String htmlFile) {
        StringBuffer sb = new StringBuffer();
        sb.append("请访问以下地址查看测试报告\n");
        sb.append(String.format("%s",htmlFile));
        return sb.toString();
    }
}
