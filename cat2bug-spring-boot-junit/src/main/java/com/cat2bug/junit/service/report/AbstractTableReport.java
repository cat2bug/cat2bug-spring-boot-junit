package com.cat2bug.junit.service.report;

import com.cat2bug.junit.service.IReport;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestWordMin;
import de.vandermeer.asciithemes.a8.A8_Grids;

/**
 * 表格报告
 */
public abstract class AbstractTableReport implements IReport<String> {

    /**
     * 获取表格头
     * @return  表头数据
     */
    public abstract String[] getTableHeader();

    /**
     * 获取表格数据段
     * @return  表格数据
     */
    public abstract String[][] getTableData();

    @Override
    public String getReport() {
        // 创建表格
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addHeavyRule();
        asciiTable.addRow(this.getTableHeader());
        asciiTable.addLightRule();
        for (String[] row : this.getTableData()) {
            asciiTable.addRow(row);
            asciiTable.addLightRule();
        }
        asciiTable.getContext().setGrid(A8_Grids.lineDoubleBlocks());
        asciiTable.getRenderer().setCWC(new CWC_LongestWordMin(20));
        return asciiTable.render();
    }
}
