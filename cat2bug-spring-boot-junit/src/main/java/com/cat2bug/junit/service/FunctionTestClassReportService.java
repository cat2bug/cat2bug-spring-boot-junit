package com.cat2bug.junit.service;

import com.cat2bug.junit.vo.HttpInterfaceVo;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能测试报告服务
 */
public class FunctionTestClassReportService {

    private static Map<String, HttpInterfaceVo> httpInterfaceMap = new ConcurrentHashMap<>();
    private static Map<Class<?>, Set<Description>> successMap = new ConcurrentHashMap<>();

    private static Map<Class<?>, Set<Failure>> failMap = new ConcurrentHashMap<>();

    private FunctionTestClassReportService() {}

    public static Map<Class<?>, Set<Description>> getSuccessDescription() {
        return successMap;
    }

    public static Map<Class<?>, Set<Failure>> getFailDescription() {
        return failMap;
    }

    public static Map<String, HttpInterfaceVo> getHttpInterfaceMap() {
        return httpInterfaceMap;
    }

    public static HttpInterfaceVo getHttpInterface(String className,String methodName) {
        String key = className+"."+methodName;
        if(httpInterfaceMap.containsKey(key))
            return httpInterfaceMap.get(key);
        else
            return null;
    }
    /**
     * 添加http接口信息
     * @param httpInterface http接口对象
     */
    public static void addHttpInterface(HttpInterfaceVo httpInterface) {
        httpInterfaceMap.put(httpInterface.getClassName()+"."+httpInterface.getMethodName(),httpInterface);
    }

    /**
     * 判断Description是否在失败数组中存在
     * @param description Description对象
     * @return  true为存在
     */
    public static boolean containsFail(Description description) {
        if(failMap.containsKey(description.getTestClass())==false) {
            return false;
        }
        return failMap.get(description.getTestClass()).stream()
                .anyMatch(d->d.getDescription().getDisplayName().equals(description.getDisplayName()));
    }
    
    public static void addSuccessDescription(Description description) {
        if(successMap.containsKey(description.getTestClass())==false){
            successMap.put(description.getTestClass(), new HashSet<>());
        }
        successMap.get(description.getTestClass()).add(description);
    }

    public static void addFailure(Failure failure) {
        if(failMap.containsKey(failure.getDescription().getTestClass())==false) {
            failMap.put(failure.getDescription().getTestClass(), new HashSet<>());
        }
        failMap.get(failure.getDescription().getTestClass()).add(failure);
    }
}
