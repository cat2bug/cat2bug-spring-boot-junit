package com.cat2bug.junit.service.report;

import com.cat2bug.junit.service.CompileClassResultService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 编译报告服务
 */
public class CompileConsoleReportService extends AbstractTableReport {

    private Map<Class<?>,Object> successClassMap = new ConcurrentHashMap<>();

    private Map<Class<?>,Throwable> failClassMap = new ConcurrentHashMap<>();

    @Override
    public String getTitle() {
        return "编译测试类报告";
    }

    @Override
    public String[] getTableHeader() {
        return new String[] {"Success Compile","Fail Compile"};
    }

    @Override
    public String[][] getTableData() {
        return new String[][] {{CompileClassResultService.getSuccessResults().size() +"",CompileClassResultService.getFailResults().size()+""}};
    }
}
