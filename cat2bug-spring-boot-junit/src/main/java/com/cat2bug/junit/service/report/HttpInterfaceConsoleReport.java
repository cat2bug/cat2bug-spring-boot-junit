package com.cat2bug.junit.service.report;

import com.cat2bug.junit.service.FunctionTestClassReportService;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Http接口测试报表
 */
public class HttpInterfaceConsoleReport  extends AbstractTableReport {
    @Override
    public String getTitle() {
        return "测试接口报告";
    }

    @Override
    public String[] getTableHeader() {
        return new String[]{"Test","Url",
                "Method Type",
//                "Request Parameter",
//                "Response",
                "Method Name"};
    }

    @Override
    public String[][] getTableData() {
        List<String[]> ret = new ArrayList<>();
        FunctionTestClassReportService.getHttpInterfaceMap().values().stream().forEach(api->{
            boolean isSuccess = false;
            Optional<Class<?>> cls = FunctionTestClassReportService.getSuccessDescription().keySet().stream().
                    filter(k->k.getName().equals(api.getClassName())).findFirst();
            if(cls.isPresent()){
                Optional<Description> d =  FunctionTestClassReportService.getSuccessDescription().get(cls.get()).stream().
                        filter(desc->desc.getMethodName().equals(api.getMethodName())).findFirst();
                if(d.isPresent())
                    isSuccess = true;
            }
            ret.add(new String[] {
                    isSuccess?"Success":"Fail",
                    api.getUrl(),
                    api.getMethodType().toString(),
//                    api.getHttpInterfaceParamVoList().stream().map(p->{
//                        return p.getClassName();
//                    }).collect(Collectors.joining("/")),
//                    "",
                    api.getClassName()+"::"+api.getMethodName()
            });
        });
        return ret.toArray(new String[0][]);
    }
}
