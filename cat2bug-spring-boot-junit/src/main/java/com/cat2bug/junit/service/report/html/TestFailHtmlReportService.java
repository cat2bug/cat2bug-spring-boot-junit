package com.cat2bug.junit.service.report.html;

import com.cat2bug.junit.service.FunctionTestClassReportService;
import com.cat2bug.junit.service.report.AbstractHtmlReport;
import com.cat2bug.junit.util.AppUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 测试失败的Html报表服务
 */
public class TestFailHtmlReportService extends AbstractHtmlReport {

    @Override
    public String getTitle() {
        return "测试方法失败报告";
    }

    @Override
    protected String getTemplateName() {
        return "test-fail.ftl";
    }

    @Override
    protected String getOutputFileName() {
        return "test-fail.html";
    }

    @Override
    protected Map getData() {
        Map ret = new HashMap();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ret.put("reportTime",sdf.format(new Date()));
        ret.put("cat2bugVersion", AppUtils.getAppVersion());

        Map<String,Object> classFailureMap = new HashMap<>();
        FunctionTestClassReportService.getFailDescription().entrySet().stream().forEach(f->{
            List<Map<String,Object>> failureSet = f.getValue().stream().map(failure->{
                Map<String,Object> retFailure = new HashMap<>();
                retFailure.put("methodName",failure.getDescription().getMethodName());
                retFailure.put("trace",failure.getTrace());
                return retFailure;
            }).collect(Collectors.toList());
            classFailureMap.put(f.getKey().getName(),failureSet);
        });
        ret.put("list",classFailureMap);
        return ret;
    }

    @Override
    protected String getReport(String htmlFile) {
        return null;
    }
}
