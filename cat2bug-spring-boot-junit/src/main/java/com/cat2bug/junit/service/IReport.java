package com.cat2bug.junit.service;

/**
 * 单元测试报告
 */
public interface IReport<T> {
    /**
     * 报告名称
     * @return  标题
     */
    String getTitle();

    /**
     * 报告
     * @return  获取报告
     */
    T getReport();
}
