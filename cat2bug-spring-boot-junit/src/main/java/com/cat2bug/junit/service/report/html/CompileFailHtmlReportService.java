package com.cat2bug.junit.service.report.html;

import com.cat2bug.junit.service.CompileClassResultService;
import com.cat2bug.junit.service.report.AbstractHtmlReport;
import com.cat2bug.junit.util.AppUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 编译失败的Html报表服务
 */
public class CompileFailHtmlReportService extends AbstractHtmlReport {

    @Override
    public String getTitle() {
        return "编译测试类失败报告";
    }

    @Override
    protected String getTemplateName() {
        return "compile-fail.ftl";
    }

    @Override
    protected String getOutputFileName() {
        return "compile-fail.html";
    }

    @Override
    protected Map getData() {
        Map ret = new HashMap();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ret.put("reportTime",sdf.format(new Date()));
        ret.put("cat2bugVersion", AppUtils.getAppVersion());

        List<Map<String,Object>> classFailureList = new ArrayList<>();
        CompileClassResultService.getFailResults().entrySet().stream().forEach(f->{
            Map<String,Object> retFailure = new HashMap<>();
            retFailure.put("className",f.getKey());
            retFailure.put("trace",f.getValue().getMessage());
            classFailureList.add(retFailure);
        });
        ret.put("list",classFailureList);
        return ret;
    }

    @Override
    protected String getReport(String htmlFile) {
        return null;
    }
}
