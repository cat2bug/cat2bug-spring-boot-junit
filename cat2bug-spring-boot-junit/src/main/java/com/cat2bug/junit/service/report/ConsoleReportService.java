package com.cat2bug.junit.service.report;

import com.cat2bug.junit.service.FunctionTestClassReportService;

import java.util.*;

/**
 * 套件结果报告服务
 */
public class ConsoleReportService extends AbstractTableReport {
    @Override
    public String getTitle() {
        return "测试类测试统计报告";
    }

    @Override
    public String[] getTableHeader() {
        return new String[] {"Test Class","Method Count","Success Count","Failure Count"};
    }

    @Override
    public String[][] getTableData() {
        List<String[]> ret = new ArrayList<>();
        FunctionTestClassReportService.getSuccessDescription().entrySet().stream().forEach(item->{
            Class<?> cls = item.getKey();
            int successCount=item.getValue().size();
            int failCount= FunctionTestClassReportService.getFailDescription().containsKey(item.getKey())?
                    FunctionTestClassReportService.getFailDescription().get(item.getKey()).size():
                    0;

            ret.add(new String[] {
                    cls.getName(),
                    String.valueOf(successCount+failCount),
                    String.valueOf(successCount),
                    String.valueOf(failCount)
            });
        });
        return ret.toArray(new String[0][]);
    }
}
