package com.cat2bug.junit.service;

public interface IRandomStringService {
    String getRandomString();
}
