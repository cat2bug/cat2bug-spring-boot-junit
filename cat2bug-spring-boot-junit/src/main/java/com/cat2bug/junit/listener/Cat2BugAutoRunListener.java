package com.cat2bug.junit.listener;

import com.cat2bug.junit.service.FunctionTestClassReportService;
import com.cat2bug.junit.service.IReport;
import com.cat2bug.junit.service.report.AbstractTableReport;
import com.cat2bug.junit.service.report.CompileConsoleReportService;
import com.cat2bug.junit.service.report.ConsoleReportService;
import com.cat2bug.junit.service.report.PushReportService;
import com.cat2bug.junit.service.report.html.CompileFailHtmlReportService;
import com.cat2bug.junit.service.report.html.HtmlReportService;
import com.cat2bug.junit.service.report.html.TestFailHtmlReportService;
import com.cat2bug.junit.util.AppUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 记录成功和失败的监听
 */
public class Cat2BugAutoRunListener extends RunListener {
	private Logger log = LogManager.getLogger(Cat2BugAutoRunListener.class);

	@Override
	public void testFinished(Description description) throws Exception {
		super.testFinished(description);
		if(FunctionTestClassReportService.containsFail(description)==false) {
			FunctionTestClassReportService.addSuccessDescription(description);
		}
	}

	@Override
	public void testFailure(Failure failure) throws Exception {
		super.testFailure(failure);
		FunctionTestClassReportService.addFailure(failure);
	}

	@Override
	public void testAssumptionFailure(Failure failure) {
		super.testAssumptionFailure(failure);
		FunctionTestClassReportService.addFailure(failure);
	}
}
