package com.cat2bug.junit.util;

import com.cat2bug.junit.type.PushReportType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Properties;

/**
 * 应用相关功能
 */
public class AppUtils {
    private final static Logger log = LogManager.getLogger(AppUtils.class);

    private static String appVersion;

    private final static Properties properties = new Properties();

    private static String tempKey="";

    static {
        try {
            loadProperties("./cat2bug.properties");
        }catch (Exception e){
            log.warn(e.getMessage());
        }
        try {
            loadProperties("./application.properties");
        }catch (Exception e){
            log.warn(e.getMessage());
        }
        try {
            loadYml("./application.yml");
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
    }

    /**
     * 加载属性
     * @param path 配置文件路径
     */
    private static void loadProperties(String path) {
        Properties pro = new Properties();
        try {
            pro.load(AppUtils.class.getClassLoader().getResourceAsStream(path));
            if(pro!=null) {
                pro.forEach((k,v)->{
                    properties.put(k,v);
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void loadYml(String path) throws IOException {
        Yaml yaml=new Yaml();
        if (Strings.isBlank(path)){
            return;
        }
        InputStream resourceAsStream = AppUtils.class.getClassLoader().getResourceAsStream(path);
        if (resourceAsStream==null){
            return;
        }
        Map loadMap = yaml.load(resourceAsStream);
        if (loadMap==null||loadMap.isEmpty()){
            resourceAsStream.close();
            return;
        }
        SwitchYmlMapToMap(loadMap);
        resourceAsStream.close();
    }

    private static void SwitchYmlMapToMap(Object map){
        if (!(map instanceof Map)) {
            String substring = tempKey.substring(0, tempKey.length() - 1);
            if(map!=null) {
                properties.put(substring, map);
            }
            return;
        }

        for (String key:((Map<String, Object>) map).keySet()){
            tempKey += key+".";
            SwitchYmlMapToMap(((Map<String, Object>) map).get(key));
            tempKey=tempKey.substring(0,tempKey.length()-(key.length()+1));
        }
    }

    /**
     * 获取推送接口地址
     * @return  接口地址
     */
    public static String getPushUrl() {
        return properties.getProperty("cat2bug.junit.push-report.host");
    }

    /**
     * 获取推送项目Key
     * @return  项目Key
     */
    public static String getPushProjectKey() {
        return properties.getProperty("cat2bug.junit.push-report.project-key");
    }

    /**
     * 获取处理人
     * @return  处理人账号
     */
    public static String getPushHandler() {
        return properties.getProperty("cat2bug.junit.push-report.handler");
    }

    /**
     * 获取版本
     * @return 版本号
     */
    public static String getPushVersion() { return properties.getProperty("cat2bug.junit.push-report.version"); }

    /**
     * 获取推送类型
     * @return  推送类型
     */
    public static PushReportType getPushType() {
        String type = properties.getProperty("cat2bug.junit.push-report.type");
        if(Strings.isBlank(type))
            return PushReportType.DEFAULT;
        switch (type.toLowerCase()) {
            case "all":
                return PushReportType.All;
            case "defect":
                return PushReportType.Defect;
            default:
                return PushReportType.DEFAULT;
        }
    }

    /**
     * 当前版本
     * @return 版本号
     */
    public static String getAppVersion() {
        return properties.getProperty("cat2bug.junit.version")+"";
   }

    /**
     * 获取文字商标
     * @return  文字商标
     */
   public static String getBanner() {
       StringBuffer sb = new StringBuffer();
       try {
           InputStream inputStream = AppUtils.class.getClassLoader().getResourceAsStream("cat2bug-junit-banner.txt");
           BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
           String line = null;
           while ((line=reader.readLine())!=null){
               sb.append(line);
               sb.append(System.lineSeparator());
           }
           reader.close();
       } catch (IOException e) {
           e.printStackTrace();
       }
       String version = getAppVersion();
       return sb.toString().replace("${cat2bug.junit.version}", version);
   }
}
