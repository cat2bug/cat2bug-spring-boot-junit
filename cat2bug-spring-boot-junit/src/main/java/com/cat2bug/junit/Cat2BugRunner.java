package com.cat2bug.junit;

import com.cat2bug.junit.listener.Cat2BugRunListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

/**
 * 基于Cat2Bug的默认Runner
 */
public class Cat2BugRunner extends BlockJUnit4ClassRunner {
	private Class<?> testClass;

	public Cat2BugRunner(Class<?> clazz) throws InitializationError {
		super(clazz);
		this.testClass = clazz;
	}

	@Override
	public void run(RunNotifier notifier) {
		notifier.addListener(new Cat2BugRunListener(this.testClass));
		super.run(notifier);
	}

}
