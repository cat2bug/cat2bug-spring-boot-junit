package com.cat2bug.junit.vo;

/**
 * 参数类型
 */
public enum HttpInterfaceParamType {
    /**
     * 请求参数
     */
    RequestParam,
    /**
     * 请求主体
     */
    RequestBody
}
