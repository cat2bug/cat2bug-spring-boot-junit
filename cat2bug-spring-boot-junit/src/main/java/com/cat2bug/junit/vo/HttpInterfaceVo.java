package com.cat2bug.junit.vo;

import java.util.List;

/**
 * Http接口
 */
public class HttpInterfaceVo {
    /**
     * 接口路径
     */
    private String url;
    /**
     * 接口方法类型
     */
    private HttpInterfaceMethodType methodType;
    /**
     * 接口类名
     */
    private String className;
    /**
     * 接口方法名
     */
    private String methodName;
    /**
     * 接口参数
     */
    private List<HttpInterfaceParamVo> httpInterfaceParamVoList;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<HttpInterfaceParamVo> getHttpInterfaceParamVoList() {
        return httpInterfaceParamVoList;
    }

    public void setHttpInterfaceParamVoList(List<HttpInterfaceParamVo> httpInterfaceParamVoList) {
        this.httpInterfaceParamVoList = httpInterfaceParamVoList;
    }

    public HttpInterfaceMethodType getMethodType() {
        return methodType;
    }

    public void setMethodType(HttpInterfaceMethodType methodType) {
        this.methodType = methodType;
    }
}
