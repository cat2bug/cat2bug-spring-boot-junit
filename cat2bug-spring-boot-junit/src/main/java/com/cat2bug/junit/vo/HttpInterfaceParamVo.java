package com.cat2bug.junit.vo;

/**
 * Http接口参数
 */
public class HttpInterfaceParamVo {
    /**
     * 参数名
     */
    private String name;
    /**
     * 参数类名
     */
    private String className;
    /**
     * 参数类型
     */
    private HttpInterfaceParamType type;
    /**
     * 参数值
     */
    private String value;

    public HttpInterfaceParamVo() {}

    public HttpInterfaceParamVo(HttpInterfaceParamType type,String name, String className) {
        this.type = type;
        this.name = name;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public HttpInterfaceParamType getType() {
        return type;
    }

    public void setType(HttpInterfaceParamType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
