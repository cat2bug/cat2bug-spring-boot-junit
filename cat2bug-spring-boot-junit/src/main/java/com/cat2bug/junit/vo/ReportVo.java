package com.cat2bug.junit.vo;

import java.util.Date;

/**
 * 报告
 */
public class ReportVo<T>
{
    /** 报告标题 */
    private String reportTitle;

    /** 报告时间 */
    private Date reportTime;

    /** 报告描述 */
    private String reportDescription;

    /** 数据类型编解码器 */
    private T reportDataCoder;

    /** 数据 */
    private T reportData;

    /** 处理人 */
    private String handler;

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }

    public String getReportDescription() {
        return reportDescription;
    }

    public void setReportDescription(String reportDescription) {
        this.reportDescription = reportDescription;
    }

    public T getReportDataCoder() {
        return reportDataCoder;
    }

    public void setReportDataCoder(T reportDataCoder) {
        this.reportDataCoder = reportDataCoder;
    }

    public T getReportData() {
        return reportData;
    }

    public void setReportData(T reportData) {
        this.reportData = reportData;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }
}
