package com.cat2bug.junit.vo;

/**
 * 接口方法类型
 */
public enum HttpInterfaceMethodType {
    GET,
    PUT,
    POST,
    DELETE
}
