package com.cat2bug.junit.vo;

import com.cat2bug.junit.type.DefectState;
import com.cat2bug.junit.type.DefectType;

import java.util.Date;
import java.util.List;

/**
 * 缺陷
 */
public class DefectVo
{
    /** 缺陷类型 */
    private DefectType defectType;
    /** 缺陷状态 */
    private DefectState defectState;

    /** 缺陷标题 */
    private String defectName;

    /** 缺陷描述 */
    private String defectDescribe;

    /** 附件 */
    private String annexUrls;

    /** 图片 */
    private String imgUrls;


    /** 测试模块名称 */
    private String moduleName;

    /** 版本 */
    private String moduleVersion;

    /** 处理人名称集合 */
    private List<String> handleByList;

    /** 处理时间 */
    private Date handleTime;

    /** 缺陷等级 */
    private String defectLevel;
    /**
     * 缺陷组标识
     */
    private String defectGroupKey;
    /**
     * 缺陷唯一标识
     */
    private String defectKey;

    public DefectType getDefectType() {
        return defectType;
    }

    public void setDefectType(DefectType defectType) {
        this.defectType = defectType;
    }

    public String getDefectName() {
        return defectName;
    }

    public void setDefectName(String defectName) {
        this.defectName = defectName;
    }

    public String getDefectDescribe() {
        return defectDescribe;
    }

    public void setDefectDescribe(String defectDescribe) {
        this.defectDescribe = defectDescribe;
    }

    public String getAnnexUrls() {
        return annexUrls;
    }

    public void setAnnexUrls(String annexUrls) {
        this.annexUrls = annexUrls;
    }

    public String getImgUrls() {
        return imgUrls;
    }

    public void setImgUrls(String imgUrls) {
        this.imgUrls = imgUrls;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleVersion() {
        return moduleVersion;
    }

    public void setModuleVersion(String moduleVersion) {
        this.moduleVersion = moduleVersion;
    }

    public List<String> getHandleByList() {
        return handleByList;
    }

    public void setHandleByList(List<String> handleByList) {
        this.handleByList = handleByList;
    }

    public Date getHandleTime() {
        return handleTime;
    }

    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    public String getDefectLevel() {
        return defectLevel;
    }

    public void setDefectLevel(String defectLevel) {
        this.defectLevel = defectLevel;
    }

    public String getDefectGroupKey() {
        return defectGroupKey;
    }

    public void setDefectGroupKey(String defectGroupKey) {
        this.defectGroupKey = defectGroupKey;
    }

    public String getDefectKey() {
        return defectKey;
    }

    public void setDefectKey(String defectKey) {
        this.defectKey = defectKey;
    }

    public DefectState getDefectState() {
        return defectState;
    }

    public void setDefectState(DefectState defectState) {
        this.defectState = defectState;
    }
}
