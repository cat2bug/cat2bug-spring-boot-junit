package com.cat2bug.junit.type;

/**
 * 缺陷状态
 */
public enum DefectState {
    /** 待处理 */
    PROCESSING,
    /** 待审核 */
    AUDIT,
    /** 已解决 */
    RESOLVED,
    /** 已驳回 */
    REJECTED,
    /** 已关闭 */
    CLOSED
}
