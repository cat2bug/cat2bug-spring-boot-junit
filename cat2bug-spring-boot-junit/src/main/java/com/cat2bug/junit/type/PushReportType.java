package com.cat2bug.junit.type;

/**
 * 推送类型
 */
public enum PushReportType {
    /**
     * 默认类型，等同于Defect
     */
    DEFAULT,
    /**
     * 推送所有检测信息
     */
    All,
    /**
     * 只推送测试出缺陷的测试方法
     */
    Defect
}
