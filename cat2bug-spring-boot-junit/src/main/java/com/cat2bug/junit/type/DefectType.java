package com.cat2bug.junit.type;

/**
 * 缺陷类型
 */
public enum DefectType {
    /** 问题 */
    BUG,
    /** 任务 */
    TASK,
    /** 需求 */
    DEMAND
}
