package com.cat2bug.junit.annotation;

import com.cat2bug.junit.type.PushReportType;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PushReport {
	/**
	 * 提交接口的系统地址
	 * @return 接口服务器地址
	 */
	String host() default "";
	/**
	 * 项目的密钥
	 * @return 密钥
	 */
	String projectKey() default "";
	/**
	 * 处理人账号
	 * @return 处理人账号
	 */
	String handler() default "";
	/**
	 * 版本号
	 * @return 测试版本
	 */
	String version() default "";
	/**
	 * 推送类型,默认只推送缺陷
	 * @return 推送类型
	 */
	PushReportType type() default PushReportType.DEFAULT;
	/**
	 * 是否推送测试报告
	 * @return 是否推送测试报告
	 */
	boolean isPush() default true;
}
