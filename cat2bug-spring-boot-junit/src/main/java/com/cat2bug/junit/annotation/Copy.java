package com.cat2bug.junit.annotation;

import java.lang.annotation.*;

/**
 * 拷贝类、方法、字段到测试类
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Copy {
}
