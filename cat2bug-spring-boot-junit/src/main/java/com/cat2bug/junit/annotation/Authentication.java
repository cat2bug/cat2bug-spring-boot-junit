package com.cat2bug.junit.annotation;

import java.lang.annotation.*;

/**
 * 身份认证
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authentication {
    String name() default "";

    String password() default "";
}
