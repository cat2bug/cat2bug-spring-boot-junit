<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>cat2bug-junit</title>
    <style>
        body {
            margin: 0px;
        }
        .title {
            width: 100%;
            display: inline-flex;
            flex-direction: row;
            align-items: center;
            background-color: #4c4c4c;
            color: #ffffff;
            padding: 5px 30px;
            margin: 0px;
        }
        .title > * {
            margin: 0px 0px 0px 20px;
            color: #ffffff;
        }
        .title > a {
            font-size: 23px;
        }
        .header-info {
            display: inline-flex;
            flex-direction: row;
        }
        .header-info > * {
            padding-right: 20px;
        }
        .report-content {
            padding: 10px 30px;
        }
        .list {
            display: flex;
            flex-direction: column;
        }
        .list > div {
            padding-bottom: 30px;
            border-bottom: 1px solid #4c4c4c;
        }
        .dot {
            position: relative;
            padding-left: 20px;
        }
        .dot::before {
            content: "";
            display: block;
            width:6px;
            height:6px;
            background-color: #2c2c2c;
            position: absolute;
            left: 0;
            top:7px;
            border-radius: 50%;
        }
    </style>
</head>
<body>
<div class="title">
    <a href="javascript:window.history.back();"><<</a>
    <h1>Cat2Bug-Spring-Boot-JUnit Test Report</h1>
</div>
<div class="report-content">
    <div class="header-info">
        <span>Version: ${cat2bugVersion}</span>
        <span>Report Time: ${reportTime}</span>
    </div>
    <div class="list">
        <#list list?keys as key>
            <div>
                <h2 id="${key}">Test Class: ${key}</h2>
                <#list list[key] as fail>
                    <div>
                        <h3 class="dot">Test Method: ${fail.methodName}</h3>
                        <span>${fail.trace}</span>
                    </div>
                </#list>
            </div>
        </#list>
    </div>
</div>
</body>
</html>