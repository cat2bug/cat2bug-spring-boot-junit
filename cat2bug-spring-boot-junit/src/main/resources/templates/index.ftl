<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>cat2bug-junit</title>
    <style>
        body {
            margin: 0px;
        }
        .title {
            background-color: #4c4c4c;
            color: #ffffff;
            padding: 5px 30px;
            margin: 0px;
        }
        .header-info {
            display: inline-flex;
            flex-direction: row;
        }
        .header-info > * {
            padding-right: 20px;
        }
        .report-content {
            padding: 10px 30px;
        }
        .list {
            display: flex;
            flex-direction: column;
        }
    </style>
</head>
<body>
<h1 class="title">Cat2Bug-Spring-Boot-JUnit Test Report</h1>
<div class="report-content">
    <div class="header-info">
        <span>Version: ${cat2bugVersion}</span>
        <span>Report Time: ${reportTime}</span>
    </div>
    <div class="list">
        <#list reports as report>
            <div>
                <h2>${report.title}</h2>
                <table border="1">
                    <#list report.tableHeader as header>
                        <th>${header}</th>
                    </#list>
                    <#list report.tableData as row>
                        <#if report.title=='功能测试结果报告'>
                            <tr>
                                <#list row as col>
                                    <#if col_index==3 && col != '0'>
                                        <td><a style="color:red;" href="test-fail.html#${row[0]}">${col}</a></td>
                                    <#elseif (col_index<=1)>
                                        <td>${col}</td>
                                    <#else>
                                        <td style="color:#a1a1a1;">${col}</td>
                                    </#if>
                                </#list>
                            </tr>
                        <#elseif report.title=='编译测试类报告'>
                            <tr>
                                <#list row as col>
                                    <#if col_index==1 && col != '0'>
                                        <td><a style="color:red;" href="compile-fail.html#${row[0]}">${col}</a></td>
                                    <#elseif col_index==0>
                                        <td>${col}</td>
                                    <#else>
                                        <td style="color:#a1a1a1;">${col}</td>
                                    </#if>
                                </#list>
                            </tr>
                        <#else >
                            <tr>
                                <#list row as col>
                                    <td>${col}</td>
                                </#list>
                            </tr>
                        </#if>
                    </#list>
                </table>
            </div>
        </#list>
    </div>
</div>
</body>
</html>